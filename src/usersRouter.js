const express = require('express')
const router = express.Router()
const { getUser, deleteUser, updatePassword } = require('./usersService')
const { passwordMiddleware } = require('./middlewares/passwordMiddleware')

router.get('/', getUser)

router.delete('/', deleteUser)

router.patch('/', passwordMiddleware, updatePassword)

module.exports = {
  usersRouter: router
}
