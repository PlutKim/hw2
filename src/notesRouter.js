const express = require('express')
const router = express.Router()
const {
  getNotes,
  addNote,
  getNoteByID,
  updateNoteByID,
  toggleNoteByID,
  deleteNoteByID
} = require('./notesService')

router.get('/', getNotes)

router.post('/', addNote)

router.get('/:id', getNoteByID)

router.put('/:id', updateNoteByID)

router.patch('/:id', toggleNoteByID)

router.delete('/:id', deleteNoteByID)

module.exports = {
  notesRouter: router
}
