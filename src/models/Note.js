const mongoose = require('mongoose')

const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    default: false,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: true
  }
})

const Note = mongoose.model('notes', noteSchema)

module.exports = {
  Note
}
