const { Note } = require('./models/Note')

const getNotes = async (req, res, next) => {
  const offset = +req.query.offset
  const limit = +req.query.limit
  try {
    const notes = await Note.find({ userId: req.user._id }, '-__v')
    res.status(200).json({
      offset: offset || 0,
      limit: limit || 0,
      count: notes.length,
      notes: notes.slice(offset, limit || notes.length)
    })
  } catch (error) {
    next(error.message)
  }
}

const addNote = async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    text: req.body.text
  })
  try {
    await note.save()
    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const getNoteByID = async (req, res) => {
  try {
    const note = await Note.findOne({ userId: req.user._id, _id: req.params.id }, '-__v')
    res.status(200).json({ note: note })
  } catch (error) {
    res.status(400).json({ message: error.message }) 
  }
}

const updateNoteByID = async (req, res) => {
  try {
    await Note.findByIdAndUpdate(
      { userId: req.user._id, _id: req.params.id },
      { text: req.body.text }
    )
    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const toggleNoteByID = async (req, res) => {
  try {
    const note = await Note.findOne({ userId: req.user._id, _id: req.params.id })
    note.completed = !note.completed
    note.save()
    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const deleteNoteByID = async (req, res) => {
  try {
    await Note.findByIdAndDelete({ userId: req.user._id, _id: req.params.id })

    res.status(200).json({ message: 'Success' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

module.exports = {
  getNotes,
  addNote,
  getNoteByID,
  updateNoteByID,
  toggleNoteByID,
  deleteNoteByID
}
