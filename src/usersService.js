const bcrypt = require('bcryptjs')
const { User } = require('./models/User')

const getUser = (req, res) => {
  const { _id, username, createdDate } = req.user
  res.status(200).json({
    user: {
      _id: _id,
      username: username,
      createdDate: createdDate
    }
  })
}

const deleteUser = (req, res) => {
  User.findByIdAndDelete(req.user._id)
    .then(() => {
      res.status(200).json({
        message: 'Success'
      })
    })
    .catch((err) => {
      res.status(400).json({
        message: 'Error'
      })
    })
}

const updatePassword = async (req, res) => {
  let { _id, oldPassword, newPassword } = req.user
  const { password: currentPassword } = await User.findById(_id)
  const isPasswordCorrect = await bcrypt.compare(
    String(oldPassword),
    String(currentPassword)
  )
  if (!isPasswordCorrect) {
    return res.status(400).json({ message: 'Invalid current password' })
  }
  newPassword = await bcrypt.hash(newPassword, 10)
  await User.findByIdAndUpdate(_id, { $set: { password: newPassword } })
    .then(() => {
      res.status(200).json({ message: 'Success' })
    })
    .catch(() => {
      res.status(400).json({
        message: 'Error'
      })
    })
}

module.exports = {
  getUser,
  deleteUser,
  updatePassword
}
